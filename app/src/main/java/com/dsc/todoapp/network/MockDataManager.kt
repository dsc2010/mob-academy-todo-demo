package com.dsc.todoapp.network

import com.dsc.todoapp.data.model.Task
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Object representing a singleton that will hold mock data for current session.
 */
object MockDataManager {

    /** Id of the user currently logged-in. Default EMPTY string. */
    val currentUserId = ""

    /** List of user with allowed access to the app. Key = username, value = password */
    val registeredUsers = HashMap<String, userEntry>()

    /** List of task by user. Key = username, value = List of tasks. */
    val userTasks = HashMap<String, ArrayList<Task>>()

    init {
        initializeRegisteredUsers()
        initializeUserTasks()
    }

    /**
     * Populate tasks for some registered users.
     */
    private fun initializeUserTasks() {
        val tasks = ArrayList<Task>()
        tasks.add(Task(id = tasks.size + 1,title = "Work on the App !!!", dateCreate =Date() ,description =   "Free some time to work on the mobile app"))
        tasks.add(Task(id = tasks.size + 1, title ="Create login page !!!", dateCreate =  Date() ,description =  "create UI MVVM and connect viewModel to repository", isCompleted = true))
        tasks.add(Task(id = tasks.size + 1, title ="get groceries",dateCreate = Date() ,description =   """
            rice,
            potato,
            cabbage,
            chocolate,
            beer
        """.trimIndent()))
        tasks.add(Task(id = tasks.size + 1,  title ="Pick up some mcDo",dateCreate =   Date() , description =  "foodie foodie foodie"))
        userTasks.set("demo", tasks)
        userTasks.set("davi", ArrayList())
    }

    /**
     * Populate initial list of registered users.
     */
    private fun initializeRegisteredUsers() {
        registeredUsers.set("demo", userEntry("pass", "Jeanette Dev"))
        registeredUsers.set("davi", userEntry("dddd", "Dav Cur"))
    }

    /**
     * Add a task for the a specific user.
     */
    fun addTask(userId: String, title: String, description: String) {
        val tasks = userTasks.get(userId)
        tasks?.add(Task(id = tasks.size + 1,title = title, dateCreate =Date() ,description =   description))
    }

    /** Data class for a mocked user entry in db. */
    data class userEntry(val password: String, val displayName: String)
}