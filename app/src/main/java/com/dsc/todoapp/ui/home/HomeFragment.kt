package com.dsc.todoapp.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dsc.todoapp.R
import com.dsc.todoapp.databinding.FragmentHomeBinding
import com.dsc.todoapp.helper.KeysUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.add_task.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding

    /** The ViewModel which will be attached to this screen and which will manage the data and logics. */
    lateinit var viewModel: HomeViewModel

    /** Factory class to instantiate [viewModel]. */
    lateinit var homeViewModelFactory: HomeViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        homeViewModelFactory = HomeViewModelFactory()
        // initiate the ViewModel. ViewModelProvider will fetch if there is an existing 1 else create 1
        // @deprecated viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(this, homeViewModelFactory).get(HomeViewModel::class.java)

        val args = HomeFragmentArgs.fromBundle(requireArguments())

        // Setting the data in viewModel so that we don't lose it in a configuration change/update.
        // TODO remove this Fragment argument when proper dependency injection is implement and fetch userDat from [LoginRepository]
        viewModel.userId = args.userId
        viewModel.userDisplayName = args.displayName
        Log.i(KeysUtils.APP_NAME, "userId: ${viewModel.userId}")
        Log.i(KeysUtils.APP_NAME, "userDisplayName: ${viewModel.userDisplayName}")

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.setLifecycleOwner(this)
        binding.pendingTaskViewModel = viewModel

        binding.textUsername.text = viewModel.userDisplayName


        initialiseAddNewTaskBottomSheet(container)

        initialiseTaskList()
        addListenerForSnackbarEvent()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initialiseAddNewTaskBottomSheet(container: ViewGroup?) {
        val bottomSheetDialog = BottomSheetDialog(context!!)

        val bottomSheetView = layoutInflater.inflate(R.layout.add_task, container, false)

        bottomSheetDialog.setContentView(bottomSheetView)

        binding.floatingActionButton.setOnClickListener {
            bottomSheetDialog.show()
        }

        bottomSheetView.btn_add.setOnClickListener {
            val titleInput = bottomSheetView.editTextTextPersonName as EditText
            val taskDescriptionInput = bottomSheetView.editTextTextMultiLine as EditText

            // TODO Break down response of addTask to success and failure style to be able to handle the two possible scenarios.
            viewModel.addTask( titleInput.text.toString(), taskDescriptionInput.text.toString())

            // We clear the inputs.
            titleInput.text.clear()
            taskDescriptionInput.text.clear()
            bottomSheetDialog.dismiss()
            Toast.makeText(context!!, "Added", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Add an event listener to be able to trigger Snackbar.
     */
    private fun addListenerForSnackbarEvent() {
        viewModel.showSnackBarEvent.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                Snackbar.make(
                    activity!!.findViewById(R.id.content),
                    "List updated",
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.doneShowingSnackbar()
            }
        })
    }

    /**
     * Initialise the taskLIst and also adding an Event listener to be update the list when a change occurred.
     */
    private fun initialiseTaskList() {
        val taskAdapter = PendingTaskAdapter()

        binding.todoList.adapter = taskAdapter

        viewModel.tasks.observe(viewLifecycleOwner, Observer {
            it?.let {
                Log.i(KeysUtils.APP_NAME, "observed changed. updating adapter.")
                taskAdapter.data = it
            }
        })

        // First call to inits the task list.
        viewModel.refreshTaskList()
    }
}