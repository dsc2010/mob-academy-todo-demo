package com.dsc.todoapp.ui.login

/**
 * User details post authentication that is exposed to the UI.
 */
data class LoggedInUserView(
    val userId: String,
    val displayName: String
)