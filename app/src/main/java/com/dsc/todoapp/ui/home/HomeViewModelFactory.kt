package com.dsc.todoapp.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dsc.todoapp.data.TaskRepository

/**
 * Factory class responsible for Instantiating [HomeViewModel]
 */
class HomeViewModelFactory : ViewModelProvider.Factory {
    /**
     * Creates a new instance of the given `Class`.
     *
     * @param modelClass a `Class` whose instance is requested
     * @param <T>        The type parameter for the ViewModel.
     * @return a newly created ViewModel
    </T> */
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(
                taskRepository = TaskRepository()
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}