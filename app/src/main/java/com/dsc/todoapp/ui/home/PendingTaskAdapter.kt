package com.dsc.todoapp.ui.home

import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dsc.todoapp.R
import com.dsc.todoapp.data.model.Task


class PendingTaskAdapter: RecyclerView.Adapter<PendingTaskAdapter.ViewHolder>() {

    /** Array list where all the data to be displayed will be stored. */
    var data = listOf<Task>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount() = data.size

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @see RecyclerView.Adapter.onCreateViewHolder
     */
    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        Log.i("todoApp", "binding view ${item.title}")
        holder.bind(item)
    }

    /**
     * View Holder for task list items.
     * @param itemView: the layout for the item.
     */
    class ViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView) {

        val taskName: TextView = itemView.findViewById(R.id.task_name)
        val taskDescription: TextView = itemView.findViewById(R.id.text_description)
        val checkboxDone: CheckBox = itemView.findViewById(R.id.checkbox_done)

        /**
         * Binding data to the elements defined in the template.
         * @param item: The [Task] assigned to the created instance of the template.
         */
        fun bind(item: Task) {
            taskName.text = item.title
            taskDescription.text = item.description
            addOnCheckChangeListener()

            checkboxDone.isChecked = item.isCompleted
            styleItemAsDone(checkboxDone.isChecked)
        }

        /**
         * Setter listener for check change on the checkbox.
         * if checked the list item will be styled as done.
         */
        private fun addOnCheckChangeListener() {
            checkboxDone.setOnCheckedChangeListener { buttonView, isChecked ->styleItemAsDone(isChecked)  }
        }

        /**
         * Style the current list item as done.
         * Strike through the Task title & set background to a disabled color.
         */
        private fun styleItemAsDone(done: Boolean) {
            if (done) {
                taskName.paintFlags = taskName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorTaskDoneBackground
                    )
                )
            } else {
                taskName.paintFlags = Paint.ANTI_ALIAS_FLAG
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorTaskPendingBackground
                    )
                )

            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.task_list_item, parent, false )
                return ViewHolder(view)
            }
        }

    }
}