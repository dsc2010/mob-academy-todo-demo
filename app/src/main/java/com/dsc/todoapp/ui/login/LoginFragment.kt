package com.dsc.todoapp.ui.login

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.dsc.todoapp.R

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = ViewModelProvider(this, LoginViewModelFactory()).get(LoginViewModel::class.java)


        // TODO replace with dataBinding
        val usernameEditText = view.findViewById<EditText>(R.id.username)
        val passwordEditText = view.findViewById<EditText>(R.id.password)
        val loginButton = view.findViewById<Button>(R.id.login)
        val loadingProgressBar = view.findViewById<ProgressBar>(R.id.loading)

        loginViewModel.loginFormState.observe(viewLifecycleOwner,
            Observer { loginFormState ->
                // pan compren the following
                if (loginFormState == null) {
                    return@Observer
                }

                // disable login button
                loginButton.isEnabled = loginFormState.isDataValid
                // we display any error message for each field if any is found (let => if not equal to null)
                loginFormState.usernameError?.let {
                    // getString() is same as getMessageFromResourceBundle by key
                    usernameEditText.error = getString(it)
                }
                loginFormState.passwordError?.let {
                    passwordEditText.error = getString(it)
                }
            })

        // Add an observe to handle the loginResult accordingly
        loginViewModel.loginResult.observe(viewLifecycleOwner,
            Observer { loginResult ->

                // return@Observer => exit observe ?
                loginResult ?: return@Observer

                // hide the loader
                loadingProgressBar.visibility = View.GONE

                //call appropriate method for success and failure
                loginResult.error?.let {
                    showLoginFailed(it)
                }
                loginResult.success?.let {
                    updateUiWithUser(it, view)
                }
            })

        // create an observer for text change
        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }


            // call the handler for the field data changes.
            override fun afterTextChanged(s: Editable) {
                // handler method is in viewModel as it contains logic.
                loginViewModel.loginDataChanged(
                    usernameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }
        }

        // adding the same created observer on username and password field.
        usernameEditText.addTextChangedListener(afterTextChangedListener)
        passwordEditText.addTextChangedListener(afterTextChangedListener)

        // adding a listener on specific action done to the password field. IME_ACTION_DONE => Done on keyboard or enter key press.
        passwordEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                intiateLogin(view, usernameEditText, passwordEditText)
            }
            false // else if action is nt the one we are expected we exit the handler by returning false
        }

        loginButton.setOnClickListener {
            loadingProgressBar.visibility = View.VISIBLE
            intiateLogin(view, usernameEditText, passwordEditText)
        }
    }

    /**
     * Login using the provided credentials after closing the soft-keyboard.
     */
    private fun intiateLogin(
        view: View,
        usernameEditText: EditText,
        passwordEditText: EditText
    ) {
        // We hide the soft keyboard
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)

        // we call the login method.
        loginViewModel.login(
            usernameEditText.text.toString(),
            passwordEditText.text.toString()
        )
    }

    /**
     * Handler function for successful login.
     */
    private fun updateUiWithUser(
        model: LoggedInUserView,
        view: View
    ) {

        // getting a formatter string resource with argument
        val welcome = getString(R.string.welcome, model.displayName)
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, welcome, Toast.LENGTH_LONG).show()

        // The following snippet is the default way to pass argument from 1 fragment to another. It is not null-safe at,
        // received argument side. so we use the safeArgs library which offers a null-safe and cleaner option.
//        val argBundle = Bundle()
//        argBundle.putString(ArgsUtils.USER_ID, model.userId )
//        argBundle.putString(ArgsUtils.DISPLAY_NAME, model.displayName )

        // we navigate to the main view.
        // The following code snippet is the default way to navigate.
        // view.findNavController().navigate(R.id.action_loginFragment_to_homeFragment2)

        // the following snippet comes from the safeArgs library. The navigation 'Directions' class is generated for every fragment used in the navigation graph.
        view.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment2(model.userId, model.displayName ))
    }

    /**
     * Handler function for failed login.
     */
    private fun showLoginFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }
}