package com.dsc.todoapp.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dsc.todoapp.data.TaskRepository
import com.dsc.todoapp.data.model.Task
import com.dsc.todoapp.helper.KeysUtils

/**
 * View Model for home screen. Home screen will display the list of all the tasks.
 * ViewModel will request and store all the logic for data to be displayed on the screen.
 */
class HomeViewModel(val taskRepository: TaskRepository): ViewModel() {


    /** Private variable that will be used to manipulate the tasks list. */
    private var _tasks =  MutableLiveData<List<Task>>()

    /** Variable to expose only the getter as only ViewModel can set/update the value of [_task]. */
    val tasks : LiveData<List<Task>>
        get() = _tasks

    /** Request a toast by setting this value to true.
     * This is private because we don't want to expose setting this value to the Fragment.
     */
    private var _showSnackbarEvent = MutableLiveData<Boolean>()

    /**
     * If this is true, immediately `show()` a toast and call `doneShowingSnackbar()`.
     */
    val showSnackBarEvent: LiveData<Boolean>
        get() = _showSnackbarEvent

    /** Username/user identifier. */
    var userId = ""

    /** The name of the user that will be displayed. Recommended format [FirstName] [LastName]. */
    var userDisplayName = ""

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     *
     * It is useful when ViewModel observes some data and you need to clear this subscription to
     * prevent a leak of this ViewModel.
     */
    override fun onCleared() {
        super.onCleared()
        Log.i(KeysUtils.APP_NAME, "${this.javaClass.name} destroyed!")
    }

    /**
     * Call this immediately after calling `show()` on a toast.
     *
     * It will clear the toast request, so if the user rotates their phone it won't show a duplicate
     * toast.
     */

    fun doneShowingSnackbar() {
        _showSnackbarEvent.value = false
    }

    /**
     * Pull the list of task from the repository using the logged user's id.
     */
    fun refreshTaskList() {
        this._tasks.value = taskRepository.getAllTasksByUserId(userId)
    }

    /**
     * Add a task to the user's todo list.
     */
    fun addTask(title: String, description: String) {
        taskRepository.addTask(userId, title, description)
        refreshTaskList()
    }


}