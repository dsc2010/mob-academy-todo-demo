package com.dsc.todoapp.data

import com.dsc.todoapp.data.model.Task
import com.dsc.todoapp.network.MockDataManager

/**
 * Repository to be the intermediary between the domain model layers and data mapping, performing the actions required on the data
 * acting in a similar way to a set of domain objects in memory.
 */
class TaskRepository {

    /**
     * Return all the Todo Task of the user currently logged-In.
     * return list of available todo if exist else return empty List.
     */
    fun getAllTasksByUserId(userId: String): ArrayList<Task> {
        return MockDataManager.userTasks.get(userId) ?: ArrayList()
    }

    /**
     * Add a task to the list for the current user.
     */
    fun addTask(userId: String, title: String, description: String) {
        MockDataManager.addTask(userId, title, description)
    }
}