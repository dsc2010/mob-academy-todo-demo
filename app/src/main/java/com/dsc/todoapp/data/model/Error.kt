package com.dsc.todoapp.data.model

/**
 * Object representing an Error model.
 */
data class Error(
    /** Unique id for the type of error. */
    val code: Int,

    /** Message to description error or additional information about the error. */
    val message: String
)