package com.dsc.todoapp.data.model

import java.util.*

/**
 * Object representing a task to be performed.
 */
data class Task(
    var id: Int,
    var title: String,
    var dateCreate: Date,
    var description: String? = "",
    var location: String? = "",
    var startDate: Date? = null,
    var endDate: Date? = null,
    var isAllDayEvent: Boolean = false,
    var isCompleted: Boolean = false
// TODO   add category
    )