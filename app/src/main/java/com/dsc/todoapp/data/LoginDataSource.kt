package com.dsc.todoapp.data

import com.dsc.todoapp.data.model.LoggedInUser

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
interface LoginDataSource {

    /**
     * Check whether credentials provided is valid.
     * @param username: the user id
     * @param password: secret text/password
     * @return [Result] with [LoggedInUser] populated in [Success] field if success else populate the [Error] field
     */
    fun login(username: String, password: String): Result<LoggedInUser>

    /**
     * Disconnect the user and revoke the authentication.
      */
    fun logout()
}

