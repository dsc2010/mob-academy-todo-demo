package com.dsc.todoapp.data.model

/**
 * Data model for login.
 */
data class Login(
    val username: String,
    val password: String
)