package com.dsc.todoapp.data.model

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository.
 * To be used on Data layer only.
 */
data class LoggedInUser(
    val userId: String,
    val displayName: String
)