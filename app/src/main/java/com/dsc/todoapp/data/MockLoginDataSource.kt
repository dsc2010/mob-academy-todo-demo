package com.dsc.todoapp.data

import com.dsc.todoapp.data.model.LoggedInUser
import com.dsc.todoapp.network.MockDataManager
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class MockLoginDataSource: LoginDataSource {



    override fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            // fetch the user from the Mock dat entries.
            val userData = MockDataManager.registeredUsers[username]
            userData.let {
                // return a new user if entry exist.
                val fakeUser = LoggedInUser(username, it!!.displayName)
                return Result.Success(fakeUser)
            }
            return Result.Error(Exception("Invalid credentials"))

        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    override fun logout() {
        // TODO: revoke authentication
    }
}